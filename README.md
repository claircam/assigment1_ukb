## I work in a team

I work on homework in a team consisting of the following people:

- Chevalier Fabien (chevafab)
- Clairet-Frier Camélia (claircam) 
- Yoshida Naoki (yoshinao)

All tasks are placed in a repository owned by <em>NameOfTheStudent</em>, which is located at <em>LinkToTheRepository</em>. 

## Repository structure

- `Modeling` - The Threat Modeling Tool
- `SecureNetwork` - network security design
- `JuiceShop` - web security
- `Privacy` - privacy protection

